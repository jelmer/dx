#!/usr/bin/make -f

RSH=/usr/bin/ssh

export DEB_BUILD_MAINT_OPTIONS=hardening=+all
DEB_CFLAGS_MAINT_APPEND=-fsigned-char -fno-strict-aliasing
DEB_CXXFLAGS_MAINT_APPEND=-fsigned-char -fno-strict-aliasing
DEB_HOST_ARCH ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)
ifeq ($(DEB_HOST_ARCH), alpha)
  DEB_CFLAGS_MAINT_APPEND+=-mieee
  DEB_CXXFLAGS_MAINT_APPEND+=-mieee
endif
export DEB_CFLAGS_MAINT_APPEND
export DEB_CXXFLAGS_MAINT_APPEND
export DEB_LDFLAGS_MAINT_APPEND=-Wl,--as-needed
export DEB_DX_ARCH = $(shell dpkg-architecture -qDEB_HOST_ARCH_OS | sed 's/^k//')

%:
	dh $@ --with autoreconf

override_dh_autoreconf:
	dh_autoreconf --as-needed

override_dh_auto_configure:
	dh_auto_configure -- \
		--prefix=/usr/share \
		--enable-smp-linux \
		--with-rsh=$(RSH) \
		--disable-dependency-tracking \
		--enable-shared
	# libtool ain't no fool, but we're being clever, relocating the libs
	# at install time. There's no --dxlibdir option to configure, alas.
	printf '%%s/^hardcode_libdir_flag_spec=.*$$/hardcode_libdir_flag_spec=" -D__LIBTOOL_AINT_NO_FOOL__ "/\nw\nq\n' | ed libtool

override_dh_auto_build:
	# Compiling displayx.c with -O2 or higher is broken on mips.
	# Limit optimisation to -O1. Likewise for zclipQ.c on all archs.
	# Upstream has a fix in place for the latter, but it doesn't work
	# when explicitly overriding CFLAGS at configure time. Therefore
	# overriding the override once again.
	# m68k is broken on too many files with -O2. [dk]
	DEB_OVERRIDE_CFLAGS="`sed -ne \
		's/^CFLAGS =\(.*\)/\1/;T;s/-O[2-9]/-O1/gp' \
		src/exec/libdx/Makefile`"; \
	$(MAKE) -C src/exec/libdx CFLAGS="$$DEB_OVERRIDE_CFLAGS" zclipQ.lo; \
	DEB_HOST_ARCH=`dpkg-architecture -qDEB_HOST_ARCH`; \
	case "$$DEB_HOST_ARCH" in \
		mips) \
			$(MAKE) -C src/exec/libdx \
				CFLAGS="$$DEB_OVERRIDE_CFLAGS" \
				displayx.lo; \
			$(MAKE); \
			;; \
		m68k) \
			$(MAKE) CFLAGS="$$DEB_OVERRIDE_CFLAGS"; \
			;; \
		*) \
			$(MAKE); \
			;; \
	esac

override_dh_auto_install:
	dh_auto_install
	# Strip paths for reproducible builds
	sed -i -e 's/^#define BSH.*/#define BSH="\/bin\/sh"/g' \
		$(CURDIR)/debian/tmp/usr/share/dx/include/dxconfig.h
	sed -i -e 's/ -f\(debug\|file\)-prefix-map=[^ ]*=\. / /g' \
		$(CURDIR)/debian/tmp/usr/share/dx/lib_*/arch.mak

override_dh_link:
	dh_link
	# ... and setup further symlinks we create by globbing.
	for i in $(CURDIR)/debian/libdx4/usr/lib/lib*.so*; do \
		i="`basename $$i`"; \
		dh_link -plibdx4 "usr/lib/$$i" "usr/lib/dx/lib_$(DEB_DX_ARCH)/$$i"; \
	done
	for i in $(CURDIR)/debian/libdx4-dev/usr/lib/lib*a; do \
		i="`basename $$i`"; \
		dh_link -plibdx4-dev "usr/lib/$$i" "usr/lib/dx/lib_$(DEB_DX_ARCH)/$$i"; \
	done

override_dh_compress:
	dh_compress -Xhtml -Xhelp
